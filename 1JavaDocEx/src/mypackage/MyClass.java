package mypackage;

/**
 * <hr>
 * This is <b>java documentation</b> example
 *
 *<hr>
 *java code example
 *<blockquote><pre>
 *			class dummy{
 *						public static final  String str="dummy string"
 *			}
 *</pre></blockquote>
 */

public class MyClass {

	/**
	 * This is <i>instance</i> string variable
	 */
	public String str="My class documentation";
	public String str2;
	
	/**
	 * MyClass Constructor
	 */
	public MyClass(String str2){
		this.str2=str2;
	}
	
	
	
    /**
     * This a static method
     */
	public static void m1() {
		int n1=100;
	}
	
	
	/**
	 * This is private method having return type int
	 * @return
	 */
	private int m4() {
		return 0;
	}
	
	public void m2() {
		final int n2=200;
	}
	
	public void m3(){
		int n3=300;
	}
	
	
	private void m5() {
		
	}
	
	/**
	 * This is public method having return type int
	 * @return 
	 */
	public int m6() {
		return 0;
	}
}
